package stepDefinitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import generic.ComActions;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Steps {

	ComActions act = new ComActions();
	private static Response response = null;
	private static String code = null;
	private Map<String, String> values = new HashMap<String, String>();

	@Given("^I make API call to fetch current weather of New York$")
	public void fetchCurrentWeather() {
		response = act.makeBasicAPICall();
	}

	@Given("^I make API call to fetch current weather of New York with following filters$")
	public void fetchCurrentWeatherWithFilters(Map<String, String> filters) {
		response = act.makeAPICallWithFilters(filters);
	}

	@When("^I check for the HTTP Status Code$")
	public void fetchStatusCode() {
		code = String.valueOf(response.getStatusCode());
	}

	@Then("^Status should be equal to \"(.*)\"$")
	public void checkStatus(String expectedCode) {
		Assert.assertEquals("incorrect status code returned", expectedCode, code);

	}

	@And("^I fetch the values for$")
	public void fetchValuesFromResponse(List<String> list) {
		JsonPath jsonPathParsor = response.jsonPath();

		for (String key : list) {
			String parent = "request.";
			if (key.contains("error")) {
				key = key.replace("error", "");
				parent = "error.";
			}
			
			values.put(key, jsonPathParsor.get(parent + key));
		}
	}

	@Then("^I verify the values$")
	public void compareResponseValues(Map<String, String> map) {

		String result = "false";
		if (map.equals(values)) {
			result = "true";
		}
		Assert.assertEquals("maps are different", "true", result);
	}

}
