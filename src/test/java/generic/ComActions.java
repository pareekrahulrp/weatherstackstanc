package generic;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ComActions {

	public String readProperties(String ObjName) {
		String propValue = null;
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			
			input = new FileInputStream("src\\test\\resources\\application.properties");			
			prop.load(input);			
			propValue=prop.getProperty(ObjName);
			
		} catch (Exception e) {				
			System.out.println("Applications properties file not found");;
		}
		
		return propValue;
	}

	
	public Response makeBasicAPICall() {
		
		String base_url = readProperties("baseUrl");
		String access_key = readProperties("accessKey");
		String end_point = readProperties("apiEndPoint");
		
		RestAssured.baseURI = base_url;
		RequestSpecification request = RestAssured.given();
		
		Response response = request.queryParam("access_key",access_key).queryParam("query","New York").get(end_point);
		System.out.println(response.asString());

		return response;
	}
	
public Response makeAPICallWithFilters(Map<String,String> filters) {
	
		String base_url = readProperties("baseUrl");
		String end_point = readProperties("apiEndPoint");
		String access_key = readProperties("accessKey");
		
		Map<String,String> query_param = new HashMap<String,String>();
		query_param.put("query", "New York");
		
		if (filters.containsKey("keyType")){
			String keyType = filters.get("keyType");
			
			switch (keyType) {
			case "invalidKey":
				access_key = readProperties(keyType);
				break;
			
			case "missingKey":
				access_key = "";
				break;
			}
			query_param.put("access_key",access_key);
		}else {
			query_param.put("access_key",access_key);
			query_param.putAll(filters);
		}
		
		System.out.println(query_param.get("units"));
				
		RestAssured.baseURI = base_url;
		RequestSpecification request = RestAssured.given();
		
		Response response = request.queryParams(query_param).get(end_point);
		System.out.println(response.asString());

		return response;
	}
}
