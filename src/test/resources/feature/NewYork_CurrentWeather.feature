#Author: Rahul Pareek
Feature: To test the various scenarios for fetching the NewYorks current weather details from weather stack api calls.

  Scenario: To test if user is able to fetch the current weather details for New York city.
    Given I make API call to fetch current weather of New York
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | query    |
      | language |
      | unit     |
    Then I verify the values
      | query    | New York, United States of America |
      | language | en                                 |
      | unit     | m                                  |

  Scenario: To test if user is able to fetch the current weather details for New York city with missing key.
    Given I make API call to fetch current weather of New York with following filters
      | keyType | missingKey |
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | errortype |
    Then I verify the values
      | type | missing_access_key |

  Scenario: To test if user is able to fetch the current weather details for New York city with invalid key.
    Given I make API call to fetch current weather of New York with following filters
      | keyType | invalidKey |
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | errortype |
    Then I verify the values
      | type | invalid_access_key |

  Scenario Outline: To test if user is able to fetch the current weather details for New York city in various unit.
    Given I make API call to fetch current weather of New York with following filters
      | units | <unit> |
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | unit |
    Then I verify the values
      | unit | <unit> |

    Examples: 
      | unit |
      | s    |
      | f    |
      
   Scenario: To test if user is able to fetch the current weather details for New York city in invalid unit.
    Given I make API call to fetch current weather of New York with following filters
      | units | a |
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | errortype |
    Then I verify the values
      | type | invalid_unit |


  Scenario Outline: To test if user is able to fetch the current weather details for New York city in various languages.
    Given I make API call to fetch current weather of New York with following filters
      | units    | <unit>     |
      | language | <language> |
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | errortype |
    Then I verify the values
      | type | function_access_restricted |

    Examples: 
      | unit | language |
      | s    | fr       |
      | f    | de       |

  Scenario Outline: To test if user is able to fetch the current weather details for New York city in various units and language.
    Given I make API call to fetch current weather of New York with following filters
      | language | <language> |
    When I check for the HTTP Status Code
    Then Status should be equal to "200"
    And I fetch the values for
      | errortype |
    Then I verify the values
      | type | function_access_restricted |

    Examples: 
      | language |
      | fr       |
      | de       |
