## Table of contents

* [General Info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Contact](#contact)

## General Info

This project is to test the various scenarios associated with the following API
// Current Weather API Endpoint
http://api.weatherstack.com/current
    ? access_key = YOUR_ACCESS_KEY
    & query = New York

* Following scenarios are covered:
  * Fetch the current weather of Newyork and validate Http Status code and default values
  * Fetch the current weather of New York using Missing and Invalid Access Keys and verify the error message
  * Fetch the current weather of New York in various units
  * Fetch the current weather of New York in various languages (Since I do not have plann to fetch the info in various languages I am getting an error.)
  * Fetch the current weather using various combination of unit and language

## Technologies

	* Java
	* Rest Assured
	* Cucumber BDD framework
	* Maven

## Setup

* To build and run the microservice at your local following should be done
  * Clone the repo on your local
  * traverse to the folder where pom.xml is located
  * Start cmd promt in this folder
  * To start and unit test the microservice run following command in cmd prompt "mvn verify" 
* 

## Contact

For any queries please contact Rahul Pareek at pareekrahulrp@gmail.com